<?php
include 'php/conexion.php';
$conexion=conexion();
$sql="SELECT id_venta,fechaVenta FROM ventas";
$result=mysqli_query($conexion,$sql);


if (isset($_POST['estado']))
{
    $estado=$_POST['estado'];
    echo $estado;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Graficos con plotly</title>
    <link rel="stylesheet"  href="librerias/bootstrap/css/bootstrap.css">
    <script src="librerias/jquery-3.5.1.min.js"></script>
    <script src="librerias/plotly-latest.min.js"></script>
</head>
<body>

<!--grafico-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel panel-heading">
                        Graficas con plotly
                    </div>
                    <div class="panel panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="cargaLineal"></div>
                            </div>
                            <div class="col-sm-6">
                                <div id="cargaBarras"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fingrafico-->

</body>
</html>

<script type="text/javascript">
$(document).ready(function(){
        $('#cargaLineal').load('lineal.php');
});

</script>