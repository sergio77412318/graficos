<?php
    require_once "php/conexion.php";
    $conexion=conexion();
    $sql="SELECT fechaVenta, montoVenta, montoVenta*2 from ventas order by fechaVenta";
    //en select las coversiones
    $result=mysqli_query($conexion,$sql);
    $valoresY=array();//montos
    $valoresX=array();//fechas
    $valoresZ=array();//montos2

    while($ver=mysqli_fetch_row($result)){
        $valoresY[]=$ver[1];
        $valoresX[]=$ver[0];
        $valoresZ[]=$ver[2];
    }

    $datosX=json_encode($valoresX);
    $datosY=json_encode($valoresY);
    $datosZ=json_encode($valoresZ);


?>
<div id="graficaLineal"></div>

<script type ="text/javascript">
    function crearCadenaLineal(json){
        var parsed=JSON.parse(json);
        var arr=[];
        for(var x in parsed){
            arr.push(parsed[x]);
        }
        return arr;
    }
</script>

<script type="text/javascript">

    datosX=crearCadenaLineal('<?php echo $datosX ?>');
    datosY=crearCadenaLineal('<?php echo $datosY   ?>');
    datosZ=crearCadenaLineal('<?php echo $datosZ ?>');


    var trace1 = {
  x: datosX,
  y: datosY,
  type: 'scatter'
};


var trace2 = {
  x: datosX,
  y: datosZ,
  type: 'scatter'
};


var data = [trace1, trace2];

Plotly.newPlot('graficaLineal', data);



</script>